const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const userSchema = new Schema({
  phone: { type: Number },
  password:  { type: String },
  authToken: [{ type: String }],
  name: { type: String }
}, {
  timestamps: true
});


module.exports = mongoose.model('User', userSchema);
