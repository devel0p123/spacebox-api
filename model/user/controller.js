const Controller = require('../../lib/controller');
const userFacade = require('./facade');
const passwordHash = require('password-hash');
const crypto = require('crypto');

class UserController extends Controller {
  createUser(req, res, next) {
    console.log("inside create user");
    console.log(req.body);
    if (req.body.password && req.body.phone) {
      // generate hashed password
      req.body.password = passwordHash.generate(req.body.password);
      // generate a new auth token
      req.body.authToken = crypto.randomBytes(16).toString('hex');

      this.facade.createUser(req.body)
      .then((doc) => {
        res.send({ success: true, user: doc });
      })
      .catch((err) => {
        res.send({ success: false, message: err });
      });
    } else {
      res.send({ success: false, message: 'Please enter phone and password' });
    }
  }

  authenticate(req, res, next) {
    console.log(req.body);
    if (req.body.password && req.body.phone) {
      // generate new auth token
      req.body.newToken = crypto.randomBytes(16).toString('hex');
      this.facade.authenticate(req.body)
      .then((doc) => {
        res.send({ success: true, result: doc });
      })
      .catch((err) => {
        console.log(err);
        res.status(400).send({ success: true, message: err });
      });
    } else {
      res.status(400).send({ success: false, message: 'Please enter phone and password' });
    }
  }

  logout(req, res, next) {
    // logout user by removing auth token from the array
    this.facade.update(
      { phone: req.user.phone },
      { $pull: { authToken: req.body.authToken } }
    )
    .then(() => {
      res.send({ success: true, message: 'User successfully logged out ' });
    });
  }
}

module.exports = new UserController(userFacade);
