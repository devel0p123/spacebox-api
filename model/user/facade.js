const Facade = require('../../lib/facade');
const userSchema = require('./schema');
const Promise = require('promise');
const passwordHash = require('password-hash');

class UserFacade extends Facade {
  // create user if user with same phone doesn't exist
  createUser(user) {
    console.log(user);
    const self = this;
    return new Promise((resolve, reject) => {
      // check for unique phone
      self.findOne({ phone: user.phone })
      .then((userFound) => {
        console.log(userFound);
        if (!userFound) {
          // if user not found then create
          self.create(user)
          .then((doc) => {
            resolve(doc);
          });
        } else {
          reject('User already exists');
        }
      });
    });
  }
  // log in a user if password and phone matches
  authenticate(user) {
    const self = this;
    return new Promise((resolve, reject) => {
      self.findOne({ phone: user.phone })
      .then((userFound) => {
        if (userFound) {
          // check hashed password is correct
          const isPassCorrect = passwordHash.verify(user.password, userFound.password);
          if (isPassCorrect) {
            // update with new auth token on login
            self.update(
              { phone: user.phone },
              { $addToSet: { authToken: user.newToken } }
            )
            .then(() => {
              resolve({ phone: user.phone, token: user.newToken, name: userFound.name, _id: userFound._id });
            });
          } else {
            reject('Password Incorrect');
          }
        } else {
          reject('User not found');
        }
      });
    });
  }
}

module.exports = new UserFacade(userSchema);
