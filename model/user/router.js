const controller = require('./controller');
const Router = require('express').Router;
const router = new Router();
const passport = require('passport');

router.route('/')
  .get((...args) => controller.find(...args))
  .post((...args) => controller.createUser(...args));

router.route('/auth')
  .post((...args) => controller.authenticate(...args));

router.route('/logout')
  .put(passport.authenticate('token', { session: false }), (...args) => controller.logout(...args));

router.route('/:id')
  .put(passport.authenticate('token', { session: false }), (...args) => controller.update(...args))
  .get(passport.authenticate('token', { session: false }), (...args) => controller.findById(...args))
  .delete(passport.authenticate('token', { session: false }), (...args) => controller.remove(...args));

module.exports = router;
